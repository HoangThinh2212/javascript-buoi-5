function sayBello() {
  console.log("Hello");
}

sayBello();
sayBello();
sayBello();
sayBello();

function sayHelloWithName(username) {
  console.log("Chào ", username);
}
sayHelloWithName("Alice");

function tinhDTB(toan, ly, hoa, username) {
  var dtb = (toan + ly + hoa) / 3;
  console.log('Chào ', username);
  console.log('dtb: ', dtb);

  // chỉ dc return về 1 giá trị
  return dtb;
}

var sv1 = tinhDTB(4,5,6,"bob");
var sv2= tinhDTB(6,7,7,"alice");
console.log('sv1: ', sv1);
console.log('sv2: ', sv2);
